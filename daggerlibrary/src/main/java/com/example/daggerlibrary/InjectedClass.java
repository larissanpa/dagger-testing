package com.example.daggerlibrary;

import javax.inject.Inject;

/**
 * Created by larissa.navarro
 * on 23/12/16.
 */

public class InjectedClass {

    @Inject
    public String name;

    public InjectedClass() {
        Library.component.inject(this);
    }

    public String getName() {
        return name;
    }
}
