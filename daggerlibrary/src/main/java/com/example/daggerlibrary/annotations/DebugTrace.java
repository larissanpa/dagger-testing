package com.example.daggerlibrary.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by larissa.navarro
 * on 23/12/16.
 */

@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.METHOD })
public @interface DebugTrace {}
