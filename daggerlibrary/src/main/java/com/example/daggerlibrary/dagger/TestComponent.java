package com.example.daggerlibrary.dagger;

import com.example.daggerlibrary.InjectedClass;

import dagger.Component;

/**
 * Created by larissa.navarro
 * on 23/12/16.
 */

@Component(modules = {
        TestModule.class
})
public interface TestComponent {

    void inject(InjectedClass injectedClass);
}
