package com.example.daggerlibrary.dagger;

import dagger.Module;
import dagger.Provides;

/**
 * Created by larissa.navarro
 * on 23/12/16.
 */

@Module
public class TestModule {

    @Provides
    public String provideName() {
        return "Dagger test!";
    }
}
