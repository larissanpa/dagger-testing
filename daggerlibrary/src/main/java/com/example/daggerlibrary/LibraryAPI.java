package com.example.daggerlibrary;

import com.example.daggerlibrary.annotations.DebugTrace;

/**
 * Created by larissa.navarro
 * on 23/12/16.
 */

public class LibraryAPI {

    @DebugTrace
    public static void init() {
        Library.init();
    }

    @DebugTrace
    public static String getName() {
        return new InjectedClass().getName();
    }
}
