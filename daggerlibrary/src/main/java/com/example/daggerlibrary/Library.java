package com.example.daggerlibrary;

import com.example.daggerlibrary.dagger.DaggerTestComponent;
import com.example.daggerlibrary.dagger.TestComponent;
import com.example.daggerlibrary.dagger.TestModule;

/**
 * Created by larissa.navarro
 * on 23/12/16.
 */
public class Library {

    public static TestComponent component;

    public static void init() {
        component = DaggerTestComponent.builder().testModule(new TestModule()).build();
    }
}
