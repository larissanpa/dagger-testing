# Dagger Testing #

Testing grounds for Dependency Injection and Aspect Oriented Programming in an Android Library Project.

DI using Dagger 2

AOP using AspectJ, based on this [post]

[post]: http://fernandocejas.com/2014/08/03/aspect-oriented-programming-in-android/