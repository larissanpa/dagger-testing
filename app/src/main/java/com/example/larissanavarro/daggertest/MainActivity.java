package com.example.larissanavarro.daggertest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.daggerlibrary.LibraryAPI;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LibraryAPI.init();

        populateTextView();
    }

    private void populateTextView() {
        TextView textView = (TextView) findViewById(R.id.dagger_text);
        if (textView != null) {
            textView.setText(LibraryAPI.getName());
        }
    }
}
